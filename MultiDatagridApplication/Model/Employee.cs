﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiDatagridApplication.Model
{
    public class Employee
    {
        public string EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public int Experiance { get; set; }

        public long MobileNumber { get; set; }

        public string Technology { get; set; }

        public string Designation { get; set; }
    }
}
