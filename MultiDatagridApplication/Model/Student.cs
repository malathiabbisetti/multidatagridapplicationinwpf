﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiDatagridApplication.Model
{
    public class Student
    {
        public string RollNo { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Class { get; set; }

        public int YearOfPass { get; set; }

        public int Percentage { get; set; }

        public string Grade { get; set; }
    }
}
