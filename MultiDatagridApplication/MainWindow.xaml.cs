﻿using MultiDatagridApplication.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiDatagridApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Employee> Employees { get; set; }

        public List<Student> Students { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            grdStudent.ItemsSource = ListOfStudents();
            grdStudent2.ItemsSource = ListOfStudents();
        }

        public void StudentMenu_Click(object sender, RoutedEventArgs e)
        {
            stkEmployeePanel.Visibility = Visibility.Hidden;
            stkStudentPanel.Visibility = Visibility.Visible;
            //stkScrollViewer.Visibility = Visibility.Visible;
            EmployeeMenu.IsChecked = false;
            StudentMenu.IsChecked = true;
            grdStudent.ItemsSource = ListOfStudents();
            grdStudent2.ItemsSource = ListOfStudents();
        }

        public void EmployeeMenu_Click(object sender, RoutedEventArgs e)
        {
            stkEmployeePanel.Visibility = Visibility.Visible;
            stkStudentPanel.Visibility = Visibility.Hidden;
            //stkScrollViewer.Visibility = Visibility.Hidden;
            EmployeeMenu.IsChecked = true;
            StudentMenu.IsChecked = false;

            Employees = new List<Employee>()
            {
                new Employee(){EmployeeId="EMP001",EmployeeName="Malathi Abbisetti",Experiance=2,MobileNumber=9505349931,Designation="Associate Analyst", Technology="DotNet"},
                new Employee(){EmployeeId="EMP002",EmployeeName="Shanmitha Grandhi",Experiance=1,MobileNumber=9989007369,Designation="Trainee", Technology="DotNet"},
                new Employee(){EmployeeId="EMP003",EmployeeName="Srinivas Rao Abbisetti",Experiance=10,MobileNumber=9912703181,Designation="Tech Lead", Technology="DotNet"},
                new Employee(){EmployeeId="EMP004",EmployeeName="Meenakshi Abbisetti",Experiance=7,MobileNumber=9505771153,Designation="Senior Analyst", Technology="DotNet"}
            };

            grdEmployee.ItemsSource = Employees;
        }

        private List<Student> ListOfStudents()
        {
            Students = new List<Student>()
            {
                new Student(){RollNo="Stu001",FirstName="Malathi",LastName="Abbisetti",Class=9,YearOfPass=2016,Grade="A"},
                new Student(){RollNo="Stu002",FirstName="Mounika",LastName="Abbisetti",Class=9,YearOfPass=2016,Grade="B"},
                new Student(){RollNo="Stu003",FirstName="Bharathi",LastName="Grandhi",Class=9,YearOfPass=2016,Grade="C"},
                new Student(){RollNo="Stu004",FirstName="Satya",LastName="Gudala",Class=9,YearOfPass=2016,Grade="A"},
                new Student(){RollNo="Stu005",FirstName="Shanmitha",LastName="Grandhi",Class=8,YearOfPass=2016,Grade="C"},
                new Student(){RollNo="Stu006",FirstName="Karthik",LastName="Abbisetti",Class=8,YearOfPass=2016,Grade="B"},
                new Student(){RollNo="Stu007",FirstName="Jethin",LastName="Grandhi",Class=7,YearOfPass=2016,Grade="A"},
                new Student(){RollNo="Stu008",FirstName="Venkat",LastName="Andey",Class=7,YearOfPass=2016,Grade="B"},
                new Student(){RollNo="Stu009",FirstName="Gopi",LastName="Andey",Class=6,YearOfPass=2016,Grade="C"},
                new Student(){RollNo="Stu010",FirstName="Venu",LastName="Abbisetti",Class=6,YearOfPass=2016,Grade="D"}
            };

            return Students.OrderByDescending(x=>x.RollNo).ToList();
        }
    }
}
